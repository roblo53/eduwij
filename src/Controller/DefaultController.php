<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/default", name="default")
     */
    public function index()
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'EduWij',
        ]);
    }

    /**
     * @Route("/cursos", name="cursos")
     */
    public function cursos()
    {
         return $this->render('default/cursos.html.twig', [
            'controller_name' => 'Cursos',
         ]);
    }
}
