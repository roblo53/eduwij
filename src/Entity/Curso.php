<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CursoRepository")
 */
class Curso
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $nombre;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="date")
     */
    private $duracion;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $precioDolar;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $precioPesoAr;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cantVideos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Video", mappedBy="idCurso")
     */
    private $videos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Inscripcion", mappedBy="idCurso")
     */
    private $inscripcions;
    /**
     * @ORM\Column(type="text")
     */
    private $urlMercadoPago;
    

    public function __construct()
    {
        $this->videos = new ArrayCollection();
        $this->inscripcions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getDuracion(): ?\DateTimeInterface
    {
        return $this->duracion;
    }

    public function setDuracion(\DateTimeInterface $duracion): self
    {
        $this->duracion = $duracion;

        return $this;
    }

    public function getPrecioDolar(): ?string
    {
        return $this->precioDolar;
    }

    public function setPrecioDolar(string $precioDolar): self
    {
        $this->precioDolar = $precioDolar;

        return $this;
    }

    public function getPrecioPesoAr(): ?string
    {
        return $this->precioPesoAr;
    }

    public function setPrecioPesoAr(string $precioPesoAr): self
    {
        $this->precioPesoAr = $precioPesoAr;

        return $this;
    }

    public function getCantVideos(): ?int
    {
        return $this->cantVideos;
    }

    public function setCantVideos(?int $cantVideos): self
    {
        $this->cantVideos = $cantVideos;

        return $this;
    }

    /**
     * @return Collection|Video[]
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function addVideo(Video $video): self
    {
        if (!$this->videos->contains($video)) {
            $this->videos[] = $video;
            $video->setIdCurso($this);
        }

        return $this;
    }

    public function removeVideo(Video $video): self
    {
        if ($this->videos->contains($video)) {
            $this->videos->removeElement($video);
            // set the owning side to null (unless already changed)
            if ($video->getIdCurso() === $this) {
                $video->setIdCurso(null);
            }
        }

        return $this;
    }

   
    public function __toString()
    {
        return $this->nombre;
    }

    /**
     * @return Collection|Inscripcion[]
     */
    public function getInscripcions(): Collection
    {
        return $this->inscripcions;
    }

    public function addInscripcion(Inscripcion $inscripcion): self
    {
        if (!$this->inscripcions->contains($inscripcion)) {
            $this->inscripcions[] = $inscripcion;
            $inscripcion->setIdCurso($this);
        }

        return $this;
    }

    public function removeInscripcion(Inscripcion $inscripcion): self
    {
        if ($this->inscripcions->contains($inscripcion)) {
            $this->inscripcions->removeElement($inscripcion);
            // set the owning side to null (unless already changed)
            if ($inscripcion->getIdCurso() === $this) {
                $inscripcion->setIdCurso(null);
            }
        }

        return $this;
    }
    public function getUrlMercadoPago(): ?string
    {
        return $this->urlMercadoPago;
    }

    public function setUrlMercadoPago(string $urlMercadoPago): self
    {
        $this->urlMercadoPago = $urlMercadoPago;

        return $this;
    }
}
